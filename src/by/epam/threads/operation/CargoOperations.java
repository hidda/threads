package by.epam.threads.operation;

import java.util.ArrayDeque;
import java.util.Random;

import org.apache.log4j.Logger;

import by.epam.threads.entity.Container;
import by.epam.threads.entity.Dock;
import by.epam.threads.entity.Ship;
import by.epam.threads.exception.LogicException;
import by.epam.threads.reporter.Reporter;

public class CargoOperations {

	private final static int SHIP_LOADED_COEFFICIENT = 4;
	private final static int SHIP_FREE_COEFFICIENT = 2;
	private final static Logger LOG = Logger.getLogger(CargoOperations.class);

	// Defines the type of cargo operation based on utilized and max capacity of
	// ship and dock
	public static void defineOperation(Ship ship, Dock dock, StringBuffer sb) {

		boolean shipIsLoaded = ship.getMaxCapacity()
				- ship.getUtilizedCapacity() <= ship.getMaxCapacity()
				/ SHIP_LOADED_COEFFICIENT;
		boolean shipIsFree = ship.getMaxCapacity() - ship.getUtilizedCapacity() >= ship
				.getMaxCapacity() / SHIP_FREE_COEFFICIENT;
		boolean dockIsEmpty = dock.getUtilizedCapacity() == 0;

		try {
			if (shipIsLoaded || dockIsEmpty) {
				int cargoSize = ship.getContainers().size();
				CargoOperations.unloadShip(ship, dock, cargoSize);
				Reporter.writeUnloadOperation(cargoSize, sb);
			} else if (shipIsFree) {
				int cargoSize = new Random().nextInt(dock.getContainers()
						.size()) + 1;
				CargoOperations.loadShip(ship, dock, cargoSize);
				Reporter.writeLoadOperation(cargoSize, sb);
			} else {
				int shipCargoSize = new Random().nextInt(ship.getContainers()
						.size()) + 1;
				CargoOperations.unloadShip(ship, dock, shipCargoSize);
				Reporter.writeUnloadOperation(shipCargoSize, sb);
				int dockCargoSize = new Random().nextInt(dock.getContainers()
						.size()) + 1;
				CargoOperations.loadShip(ship, dock, dockCargoSize);
				Reporter.writeLoadOperation(dockCargoSize, sb);
			}
		} catch (LogicException e) {
			LOG.warn("Operation exception: " + e.getMessage());
		} finally {
			ship.setUtilizedCapacity();
			dock.setUtilizedCapacity();
		}
	}

	// Moves containers from the ship to the dock
	public static void unloadShip(Ship ship, Dock dock, int cargoSize)
			throws LogicException {

		ArrayDeque<Container> dockCargo = dock.getContainers();
		ArrayDeque<Container> shipCargo = ship.getContainers();

		for (int i = 0; i < cargoSize; i++) {
			Container container = shipCargo.pollFirst();
			if (dock.getUtilizedCapacity() + container.getWeight() < dock
					.getMaxCapacity()) {
				dockCargo.add(container);
			} else {
				throw new LogicException(
						"Unable to add another container to the dock "
								+ dock.getId() + " from " + ship.getShipName());
			}
		}
	}

	// Moves number of containers from the dock to the ship
	public static void loadShip(Ship ship, Dock dock, int cargoSize)
			throws LogicException {

		ArrayDeque<Container> dockCargo = dock.getContainers();
		ArrayDeque<Container> shipCargo = ship.getContainers();

		for (int i = 0; i < cargoSize; i++) {
			Container container = dockCargo.pollFirst();
			if (ship.getUtilizedCapacity() + container.getWeight() <= ship
					.getMaxCapacity()) {
				shipCargo.add(container);
			} else {
				throw new LogicException(
						"Unable to add another container to the ship "
								+ ship.getShipName() + " from " + dock.getId());
			}
		}
	}
}

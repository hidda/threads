package by.epam.threads.entity;

import java.util.Random;

public class Container {

	private int weight;
	private final static int CONTAINER_WEIGHT_BASE = 5;
	private final static int CONTAINER_WEIGHT_COEFFICIENT = 20;

	public Container() {
		this.setWeight();
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight() {
		this.weight = ((new Random().nextInt(CONTAINER_WEIGHT_BASE)) + 1) * CONTAINER_WEIGHT_COEFFICIENT;
	}

}

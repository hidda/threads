package by.epam.threads.entity;

import java.util.ArrayDeque;
import java.util.Random;

import org.apache.log4j.Logger;

import by.epam.threads.exception.ResourceException;
import by.epam.threads.operation.CargoOperations;
import by.epam.threads.reporter.Reporter;

public class Ship extends Thread {

	private final static Logger LOG = Logger.getLogger(Ship.class);
	private final static int SHIP_CAPACITY = 3000;
	private final static int MAX_CONTAINER_NUMBER = 30;

	private int shipId;
	private String shipName;
	private int maxCapacity;
	private int utilizedCapacity;
	private ArrayDeque<Container> containers = new ArrayDeque<Container>();
	private Port port;

	public Ship(int shipId, String shipName) {
		this.shipId = shipId;
		this.shipName = shipName;
		this.setMaxCapacity();
		this.setContainers();
		this.setUtilizedCapacity();
	}

	public void setPort(Port port) {
		this.port = port;
	}

	public void setContainers() {
		for (int i = 0; i < ((new Random().nextInt(MAX_CONTAINER_NUMBER)) + 1); i++) {
			this.containers.add(new Container());
		}
	}

	public ArrayDeque<Container> getContainers() {
		return containers;
	}

	public int getShipId() {
		return shipId;
	}

	public void setShipId(int shipId) {
		this.shipId = shipId;
	}

	public String getShipName() {
		return shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public int getMaxCapacity() {
		return maxCapacity;
	}

	public void setMaxCapacity() {
		this.maxCapacity = SHIP_CAPACITY;
	}

	public int getUtilizedCapacity() {
		return utilizedCapacity;
	}

	public void setUtilizedCapacity() {
		int sum = 0;
		for (Container c : containers) {
			sum += c.getWeight();
		}
		this.utilizedCapacity = sum;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Ship Id: " + this.shipId + "; ");
		sb.append("Ship Name: " + this.shipName + "; ");
		sb.append("Ship Max Capacity: " + this.maxCapacity + "; ");
		sb.append("Ship Utilized Capacity: " + this.utilizedCapacity + "; ");
		sb.append("Ship Contains " + this.containers.size() + " containers.");
		return sb.toString();
	}

	@Override
	public void run() {
		if (this.port != null) {
			try {
				Dock dock = port.getDock();
				StringBuffer sb = new StringBuffer();
				Reporter.writeBeforeCondition(this, dock, sb);
				CargoOperations.defineOperation(this, dock, sb);
				Reporter.writeAfterCondition(this, dock, sb);
				Reporter.appendReport(sb);
				port.returnDock(dock);
			} catch (ResourceException e) {
				LOG.error("Thread exception: " + e);
			}
		} else {
			LOG.error("Unable to proceed the operation because Port Object is null;");
		}
	}
}

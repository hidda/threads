package by.epam.threads.entity;

import java.util.ArrayDeque;
import java.util.Random;

public class Dock {

	private final static int DOCK_CAPACITY = 5000;
	private final static int MAX_CONTAINER_NUMBER = 50;
	
	private int id;
	private int maxCapacity;
	private int utilizedCapacity;
	private ArrayDeque<Container> containers = new ArrayDeque<Container>();

	public Dock(int id) {
		this.id = id;
		this.setMaxCapacity();
		this.setContainers();
		this.setUtilizedCapacity();
	}

	public void setContainers() {
		for (int i = 0; i < (new Random().nextInt(MAX_CONTAINER_NUMBER) + 1); i++) {
			this.containers.add(new Container());
		}
	}
	
	public ArrayDeque<Container> getContainers() {
		return containers;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMaxCapacity() {
		return maxCapacity;
	}

	public void setMaxCapacity() {
		this.maxCapacity = DOCK_CAPACITY;
	}

	public int getUtilizedCapacity() {
		return utilizedCapacity;
	}

	public void setUtilizedCapacity() {
		int sum = 0;
		for (Container c : containers) {
			sum += c.getWeight();
		}
		this.utilizedCapacity = sum;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Dock Id: " + this.id + "; ");
		sb.append("Dock Max Capacity: " + this.maxCapacity + "; ");
		sb.append("Dock Utilized Capacity: " + this.utilizedCapacity + "; ");
		sb.append("Dock Contains " + this.containers.size() + " containers.");
		return sb.toString();
	}
}

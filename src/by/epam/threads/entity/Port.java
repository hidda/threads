package by.epam.threads.entity;

import java.util.ArrayDeque;
import java.util.concurrent.Semaphore;

import by.epam.threads.exception.ResourceException;

public class Port {

	private final static int PORT_SIZE = 7;
	
	private ArrayDeque<Dock> docks = new ArrayDeque<Dock>();
	private final Semaphore semaphore = new Semaphore(PORT_SIZE, true);

	public Port() {
		this.docks = buildPort();
	}

	private ArrayDeque<Dock> buildPort() {
		for (int i = 0; i < PORT_SIZE; i++) {
			docks.add(new Dock(i + 1));
		}
		return docks;
	}

	public Dock getDock() throws ResourceException {
		Dock dock = null;
		while (true) {
			try {
				if (semaphore.tryAcquire()) {
					dock = docks.poll();
					return dock;
				} else {
					Thread.sleep(500);
					continue;
				}
			} catch (InterruptedException e) {
				throw new ResourceException();
			}
		}
	}

	public void returnDock(Dock dock) {
		docks.add(dock);
		semaphore.release();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Dock d : docks) {
			sb.append(d.toString());
			sb.append(System.lineSeparator());
		}
		sb.append("Port size: " + PORT_SIZE);
		return sb.toString();
	}
}

package by.epam.threads.runner;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;

import by.epam.threads.entity.Port;
import by.epam.threads.entity.Ship;
import by.epam.threads.reporter.Reporter;

public class Runner {

	private final static String CONFIG_NAME = "config/log4j.xml";

	static {
		new DOMConfigurator().doConfigure(CONFIG_NAME,
				LogManager.getLoggerRepository());
	}

	public static void main(String[] args) throws InterruptedException {

		Port port = new Port();
		FileWriter fw = Reporter.createFileWriter();

		List<Ship> ships = new ArrayList<Ship>();
		boolean allDead = false;

		for (int i = 0; i < 30; i++) {
			Ship ship = new Ship((i + 1), "Ship #" + (i + 1));
			ship.setPort(port);
			ships.add(ship);
			ship.start();
		}

		while (true) {
			Thread.sleep(1000);
			for (Ship ship : ships) {
				if (!ship.isAlive()) {
					allDead = true;
				} else {
					allDead = false;
					break;
				}
			}
			if (!allDead) {
				continue;
			} else {
				Reporter.writeReport(fw);
				Reporter.closeWriter(fw);
				break;
			}
		}
	}
}

package by.epam.threads.reporter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.log4j.Logger;

import by.epam.threads.entity.Dock;
import by.epam.threads.entity.Ship;

public class Reporter {

	private static File file = new File("report\\report.txt");
	private final static Logger LOG = Logger.getLogger(Reporter.class);
	private static StringBuffer report = new StringBuffer();

	public static FileWriter createFileWriter() {
		FileWriter fw = null;
		try {
			fw = new FileWriter(file);
		} catch (IOException e) {
			LOG.error("Unable to create FileWriter;");
		}
		return fw;
	}

	public static void appendReport(StringBuffer sb) {
		report.append(sb);
	}

	public static void writeBeforeCondition(Ship ship, Dock dock,
			StringBuffer sb) {
		sb.append("Ship " + ship.getShipId() + " arrived to dock "
				+ dock.getId());
		sb.append(System.lineSeparator());
		sb.append("Before exchange: " + ship.toString());
		sb.append(System.lineSeparator());
		sb.append("Before exchange: " + dock.toString());
		sb.append(System.lineSeparator());
	}

	public static void writeAfterCondition(Ship ship, Dock dock, StringBuffer sb) {
		sb.append("After exchange: " + dock.toString());
		sb.append(System.lineSeparator());
		sb.append("After exchange: " + ship.toString());
		sb.append(System.lineSeparator());
		sb.append("Ship " + ship.getShipId() + " left dock " + dock.getId());
		sb.append(System.lineSeparator());
		sb.append(System.lineSeparator());
	}

	public static void writeUnloadOperation(int cargoSize,
			StringBuffer sb) {
		sb.append("Ship was unloaded: " + cargoSize + " was moved to the dock");
		sb.append(System.lineSeparator());
	}

	public static void writeLoadOperation(int cargoSize, StringBuffer sb) {
		sb.append("Ship was loaded: " + cargoSize + " containers was moved to the ship");
		sb.append(System.lineSeparator());
	}

	public static void writeReport(FileWriter fw) {
		try {
			fw.write(report.toString());
			fw.write(System.lineSeparator());
		} catch (IOException e) {
			LOG.error("Unable to write report;");
		}
	}

	public static void closeWriter(FileWriter fw) {
		if (fw != null) {
			try {
				fw.close();
			} catch (IOException e) {
				LOG.warn("Closing report exception;");
			}
		}
	}
}
